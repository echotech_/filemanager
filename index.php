<?php 
    session_start();
    if (!isset($_SESSION['sid']) && !isset($_SESSION['userName'])) {
        header('Location: login.php');
    }
    require_once('util/functions.php');

    $userPath = 'storage/' . $_SESSION['userName'];
    $path = isset($_POST['path']) ? getPath($_POST['path']) : $userPath;
    
    require('./ui_components/htmlHead.php'); 
?>
<body>
    <?php require('./ui_components/navbar.php');  ?>
    <div class="container mt-4 mb-2">
        <div class="row">
        <!-- PARENT FOLDER -->
        <?php if ($path !== $userPath): ?>
            <div class="col-2 col-lg-1 col-md-1">
                <button 
                    type="submit" 
                    name="file" 
                    value="<?=dirname($path); ?>" 
                    class="link-button changeDir mr-3">
                    <i class="fas fa-arrow-circle-left"></i>
                </button>
            </div>
        <?php endif; ?>
            <div class="col-4 col-lg-2 col-md-2">
                <button type="button" name="makeNewFolder" value="<?=$path; ?>" class="link-button modFile" data-toggle="modal" data-target="#myModal">
                    <i class="fas fa-folder-plus"></i>
                </button>
                <button type="button" class="link-button uploadToggle">
                    <i class="fas fa-upload"></i>
                </button>
            </div>
            <div class="col-6 col-lg-3 col-md-3">
                <button type="submit" name="download" class="link-button bulkAction" id="downloadSelected">
                    <i class="fas fa-cloud-download-alt"></i>
                </button>
                <button type="button" name="deleteFile" class="link-button bulkAction" id="deleteSelected" data-target="#myModal">
                    <i class="fas fa-trash-alt"></i>
                </button>
                <button type="submit" name="copyMoveFile" value="<?=$path; ?>" class="link-button bulkAction" id="copyMoveSelected">
                    <i class="far fa-copy"></i>
                </button>
            </div>
        </div>


        <!-- hidden card for upload -->
        <div id="uploadCard" class="card border-primary mt-3" style="max-width: 30rem; display: none;">
            <div class="card-body">
                <h4 class="card-title">
                    Choose file to upload
                    <button type="button" class="close uploadToggle">&times;</button>
                </h4>
                <form action="./actions/uploadFile.php" method="POST" enctype="multipart/form-data">
                    <input type="hidden" class="cwd" name="cwd" value="<?=dirname($_SERVER['PHP_SELF']); ?>">
                    <div class="form-group">
                        <input type="file" name="uploadFile[]" id="uploadFile" multiple="multiple" >
                    </div>
                    <div class="form-group" id="fileList">
                        
                    </div>
                    <div class="form-group">
                        <button type="submit" id="uploadBtn" name="path" class="btn btn-primary" value="<?=$path; ?>">
                            Upload
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- Docs: https://datatables.net/manual/ -->
    <div class="container" id="dtContainer">
        <form id="datatableForm" method="post" class="inline" action="">
            <input type="hidden" class="cwd" name="cwd" value="<?=dirname($_SERVER['PHP_SELF']); ?>">
            <table id="dataTable" class="table table-hover display" style="width: 100%;">
                <thead>
                    <tr>
                        <th></th>
                        <th>File name</th>
                        <th>Size</th>
                        <th>Last modified time</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
    <?php foreach (new DirectoryIterator($path) as $file): ?>
    <?php 
        if ($file->isDot()) continue;
        $pathName = $file->getPathname();
        $size = getPathSize($file);
        $modTime = getModTime($file);
    ?>
                    <tr>
                        <td><?=$pathName; ?></td>
                        <td>
                            <button 
                                type="submit" 
                                name="file" 
                                value="<?=$pathName; ?>" 
                                class="link-button <?=is_file($pathName) ? "preview" : "changeDir"; ?>">
                                <?=$file; ?>
                            </button>
                        </td>
                        <td><?=$size; ?></td>
                        <td><?=$modTime; ?></td>
                        <td>
                            <!-- ACTIONS -->
                            <button type="submit" name="download" value="<?=$pathName; ?>" class="btn downloadFile">
                                <i class="fas fa-cloud-download-alt"></i>
                            </button>
                            <button type="button" name="renameFile" value="<?=$pathName; ?>" class="btn modFile" data-toggle="modal" data-target="#myModal">
                                <i class="fa fa-edit"></i>
                            </button>
                            <button type="button" name="deleteFile" value="<?=$pathName; ?>" class="btn modFile" data-toggle="modal" data-target="#myModal">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                            <button type="submit" name="copyMoveFile" value="<?=$pathName; ?>" class="btn copyMoveFile">
                                <i class="far fa-copy"></i>
                            </button>
                        </td>
                    </tr>
    <?php endforeach; ?>
                </tbody>
            </table>
        </form>
        <?php require('./ui_components/modal.php') ?>
    </div>

    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">
    </script>
    <script src="https://kit.fontawesome.com/16f806e951.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.1/js/buttons.bootstrap4.min.js"></script>
    <script type="text/javascript" src="//gyrocode.github.io/jquery-datatables-checkboxes/1.2.11/js/dataTables.checkboxes.min.js"></script>
    <script src="./js/dataTableScript.js"></script>
    <script src="./js/indexScript.js"></script>
</body>
</html>