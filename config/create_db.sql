CREATE DATABASE IF NOT EXISTS filemanager;
USE filemanager;
DROP TABLE IF EXISTS users;

CREATE TABLE users (
u_id INT(6) NOT NULL AUTO_INCREMENT PRIMARY KEY,
u_name VARCHAR(30) NOT NULL UNIQUE,
u_pword VARCHAR(255) NOT NULL
);

INSERT INTO users (u_id, u_name, u_pword) VALUES
(1, 'user1', '12345'), (2, 'user2', '123456');
