<?php
session_start();
require_once('./util/functions.php');

$path = false;
$paths = null;
$newPath = false;

if (isset($_POST['paths'])) {
    $paths = $_POST['paths'];
    $paths = array_map('getPath', $paths);
} elseif (isset($_POST['path'])) {
    $path = getPath($_POST['path']);
}

if (isset($_POST['newPath'])) {
    $newPath = getPath($_POST['newPath']);
} elseif (isset($_POST['path'])) {
    $newPath = dirname($path);
} elseif (isset($_POST['paths'])) {
    $newPath = dirname($paths[0]);
}
if ($newPath === false || ($path === false && $paths == null)) {
    header('Location: ./');
}
$userPath = 'storage/' . $_SESSION['userName'];

require_once('./ui_components/htmlHeadMin.php');
?>

<body>

<?php require('./ui_components/navbar.php'); ?>
<div class="container mt-4">
    <h3>Copying/Moving</h3>
    <div class="container ml-3">
        <p class="lead">Source file: <?=getPath($path);?></p>
        <p class="lead">Destination path: <?=getPath($newPath);?></p>
    </div>
    <h4>Choose new directory</h4>
</div>
<!-- PARENT FOLDER -->

<div class="container">
        <form id="copyMoveForm" method="post" class="inline" action="./copyMovePage.php">
            <input type="hidden" id="cwd" name="cwd" value="<?=dirname($_SERVER['PHP_SELF']); ?>">
            <input type="hidden" id="destPath" name="destPath" value="<?=$newPath; ?>">
        <?php if ($path): ?>
            <input type="hidden" id="path" name="path" value="<?=$path; ?>">
        <?php elseif ($paths): ?>
            <?php foreach ($paths as $pathsElement): ?>
                <input type="hidden" name="paths[]" value="<?=$pathsElement; ?>">
            <?php endforeach; ?>
        <?php endif; ?>

            <div class="container m-3">
                <ul class="list-group">
                <?php foreach (new DirectoryIterator($newPath) as $file): ?>
                <?php 
                    $pathName = $file->getPathname();
                    if ($file == '.' || $file->isFile()) continue;
                    if ($file == '..' && $newPath === $userPath) continue;
                ?>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <button 
                            type="submit" 
                            name="newPathBtn" 
                            value="<?=$file != '..' ? $pathName : dirname(dirname($pathName)); ?>" 
                            class="link-button newPath">
                            <?=$file != '..' ? $file : $file . ' back to parent folder'; ?>
                        </button>
                    </li>
                <?php endforeach; ?>
                </ul>
            </div>
            <div class="container m-3">
                <button type="submit" name="copyFile" class="btn btn-primary confirmCopyMove">Copy</button>
                <button type="submit" name="moveFile" class="btn btn-primary confirmCopyMove ">Move</button>
                <button type="submit" class="btn btn-secondary cancelCopyMove">Cancel</button>
            </div>
        </form>
    </div>

    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="./js/copyMoveScript.js"></script>
    <script src="https://kit.fontawesome.com/16f806e951.js" crossorigin="anonymous"></script>
</body>
</html>