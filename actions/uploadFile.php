<?php
session_start();
require('../util/functions.php');
if (!isset($_SESSION['sid']) || !isset($_SESSION['userName'])
    || !isset($_POST['path']) || !safePath($_POST['path'])) {
    header('Location: ../');
}
$uploadOk = 1;

$total = count($_FILES["uploadFile"]["name"]);

$target_dir = '../'. $_POST['path'];

// Loop through each file
for($i = 0; $i < $total; $i++) {
    //Get the temp file path
    $tmpFilePath = $_FILES['uploadFile']['tmp_name'][$i];

    if ($tmpFilePath != ""){
        //Setup our new file path
        $target_file = $target_dir . '/' . basename($_FILES["uploadFile"]["name"][$i]);

        //Check if file already exists
        while (file_exists($target_file)) {
            $pathinfo = pathinfo($target_file);
            $target_file = $target_dir . '/' . $pathinfo['filename'] . '(1).' . $pathinfo['extension'];
        }
    
        //Upload the file into the temp dir
        if (move_uploaded_file($tmpFilePath, $target_file)) {
            header('Location: ../');
            // echo "The file ". basename($target_file). " has been uploaded.\n";
            // echo "Redirecting to homepage...";
        } else {
            echo "Sorry, there was an error uploading your file.";
        }
    }
}

header('Refresh: 2; url=../');
?>