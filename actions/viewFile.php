<?php
session_start();
require('../util/functions.php');
if (!isset($_SESSION['sid']) || !isset($_SESSION['userName'])
    || !isset($_POST['path']) || !safePath($_POST['path'])) {
    header('Location: ../');
}
$path = '../' . $_POST['path'];

$type = mime_content_type($path);


if (strpos($type, 'audio') === false && strpos($type, 'video') === false) {
    header('Content-Type:'.$type);
    header('Content-Length: ' . filesize($path));
    header('Content-Disposition: filename="' . pathinfo($path, PATHINFO_BASENAME));
    header('X-Pad: avoid browser bug');
    header('Cache-Control: must-revalidate');

    readfile($path);
} elseif (is_file($path)) {
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');//change your extension of your files
    header('Content-Disposition: attachment; filename="'.basename($path).'"');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($path));
    readfile($path);
    exit;
}