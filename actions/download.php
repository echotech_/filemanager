<?php
session_start();
if (!isset($_SESSION['sid']) || !isset($_SESSION['userName'])) {
    header('Location: ../');
}
require_once('../util/functions.php');

// FROM CHECKBOXES SELECTED PATHS
if (isset($_POST['paths'])) {
    $paths = $_POST['paths'];
    $dirname = "temp_folder";

    while (is_dir($dirname)) {
        $dirname .= '(1)';
    }

    if (!mkdir($dirname)) echo "Error in mkdir";

    foreach ($paths as $path) {
        if (!safePath($path)) {
            header('Location: ../');
        }
        $path = '../' . $path;
        
        $destPath = $dirname . '/' . basename($path);

        if (is_dir($path)) {
            rcopydir($path, $destPath);
        } else {
            copy($path, $destPath);
        }
    }

    downloadItem($dirname);
    rrmdir($dirname);
} elseif (isset($_POST['path']) && safePath($_POST['path'])) {
    $path = '../' . $_POST['path'];
    downloadItem($path);
} else {
    header('Location: ../');
}