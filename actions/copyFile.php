<?php
session_start();
if (!isset($_SESSION['sid']) || !isset($_SESSION['userName'])) {
    header('Location: ../');
}
require('../util/functions.php');
require('../ui_components/htmlHeadMin.php'); 
?>
<body>
<?php require('../ui_components/navbar.php'); ?>

<?php
$error = false;

if (isset($_POST['path']) && isset($_POST['destPath'])
    && safePath($_POST['path']) && safePath($_POST['destPath'])) {
    $srcPath = '../' . $_POST['path'];
    $targetDir = '../'. $_POST['destPath'];
    $destPath = $targetDir . '/' . basename($_POST['path']);

    // file exists
    while (file_exists($destPath)) {
        $pathinfo = pathinfo($destPath);
        $destPath = $targetDir . '/' . $pathinfo['filename'] . '(1).' . $pathinfo['extension'];
    }

    if (is_dir($srcPath)) {
        rcopydir($srcPath, $destPath);
    } elseif (!copy($srcPath, $destPath)) {
        $error = true;
    }
} elseif (isset($_POST['paths']) && isset($_POST['destPath']) && safePath($_POST['destPath'])) {
    $total = count($_POST['paths']);
    $targetDir = '../'. $_POST['destPath'];

    // Loop through each file
    for($i = 0; $i < $total; $i++) {
        if (!safePath($_POST['paths'][$i])) {
            header('Location: ../');
        }
        $srcPath = '../' . $_POST['paths'][$i];
        $destPath = $targetDir . '/' . basename($_POST['paths'][$i]);

        // file exists
        while (file_exists($destPath)) {
            $pathinfo = pathinfo($destPath);
            $destPath = $targetDir . '/' . $pathinfo['filename'] . '(1).' . $pathinfo['extension'];
        }

        if (is_dir($srcPath)) {
            rcopydir($srcPath, $destPath);
        } elseif (!copy($srcPath, $destPath)) {
            $error = true;
        }
    }
} else {
    header('Location: ../');
}
?>

<div style="padding:100px;">
    <div class="card text-white bg-danger mb-3" style="max-width: 20rem;margin:auto"> 
    <div class="card-header">
        <center>
            <?=$error ? 
            'Oh Snap! The file cannot be copied.' : 
            'The file(s) has been copied successfully.'; ?>
        </center>
    </div>
    </div>
    </div> 
    <blockquote class="blockquote text-center">
    <p class="mb-0">You will be redirected to the home page in 2.0s.</p>
    </blockquote>
</div>
</body>
</html>
<?php 
header('Refresh: 2; url=../');
?>