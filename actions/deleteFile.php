<?php
session_start();
require('../util/functions.php');
if (!isset($_SESSION['sid']) || !isset($_SESSION['userName'])) {
    header('Location: ../');
}

if (isset($_POST['path'])) {
    if (!safePath($_POST['path'])) {
        header('Location: ../');
    }

    $path = '../' . $_POST['path'];
    if (is_dir($path)) {
        rrmdir($path);
    } else {
        unlink($path);
    }

    echo json_encode(["msg" => "Deleted successfully", "success" => true]);
} elseif (isset($_POST['paths'])) {
    $paths = $_POST['paths'];

    foreach ($paths as $path) {
        if (!safePath($path)) {
            header('Location: ../');
        }
        $path = '../' . $path;

        if (is_dir($path)) {
            rrmdir($path);
        } else {
            unlink($path);
        }
    }

    header('Location: ../');
} else {
    header('Location: ../');
}