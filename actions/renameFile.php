<?php
session_start();
require('../util/functions.php');
if (!isset($_SESSION['sid']) || !isset($_SESSION['userName'])
    || !isset($_POST['srcPath']) || !safePath($_POST['srcPath'])
    || !isset($_POST['destPath'])) {
    header('Location: ../');
}
$srcPath = '../' . $_POST['srcPath'];
$destPath =  dirname($srcPath) . '/'. $_POST['destPath'];

if (file_exists($destPath)) {
    echo json_encode(["msg" => "File with same name already exist!", "success" => false]);
} elseif (!rename($srcPath, $destPath)) {
    echo json_encode(["msg" => "Unknown error occurred! Please contact the system admin!", "success" => false]);
} else {
    echo json_encode(["msg" => "File renamed successfully.", "success" => true]);
}