<?php
session_start();
require('../util/functions.php');
if (!isset($_SESSION['sid']) || !isset($_SESSION['userName'])
    || !isset($_POST['path']) || !safePath($_POST['path'])) {
    header('Location: ../');
}
$path = '../' . $_POST['path'];

if (is_dir($path)) {
    echo json_encode(["msg" => "Folder with same name already exist!", "success" => false]);
} elseif (!mkdir($path)) {
    echo json_encode(["msg" => "Unknown error occurred! Please contact the system admin!", "success" => false]);
} else {
    echo json_encode(["msg" => "Folder create successfully.", "success" => true]);
}