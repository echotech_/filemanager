<nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
    <a class="navbar-brand" href="./">EchoTech Files</a>
    <a class="navbar-brand ml-auto" href="./actions/logoutAction.php">
        <i class="fas fa-sign-out-alt"></i>
    </a>
</nav>