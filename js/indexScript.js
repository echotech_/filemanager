$(document).ready(function() {
    const form = $('#datatableForm');

    $('.bulkAction').click((e) => {
        if ($('input[name="paths[]"]').length == 0) {
            return;
        }

        const elemId = $(e.currentTarget).attr('id');

        switch (elemId) {
            case 'downloadSelected':
                form.attr('action', './actions/download.php');
                form.submit();
                $('input[name="paths[]"]').remove();
                break;
            case 'deleteSelected':
                // Add modal on this one
                let title, body, total;
                total = $('input[name="paths[]"]').length;
                title = 'Delete';
                body = `Confirm deleting ${total} selected files?`;

                $('#myModal').modal('show');

                $('.modal-title').text(title);
                $('.modal-body').html(body);

                // MODAL CONFIRMATION
                $('#modalConfirm').click((e) => {
                    // e.preventDefault();

                    // REPLACE THIS WITH DELETEFILE.PHP AFTER DONE
                    form.attr('action', './actions/deleteFile.php');
                    form.submit();
                    $('input[name="paths[]"]').remove();
                });
                break;
            case 'copyMoveSelected':
                form.attr('action', './copyMovePage.php');
                form.submit();
                $('input[name="paths[]"]').remove();
                break;
            default:
                break;
        }
        $('#myModal').on('hidden.bs.modal' , () => {
            $('input[name="paths[]"]').remove();
        })
    });

    $('.uploadToggle').click((e) => {
        $('#uploadCard').toggle();
    });

    $(".modFile").click(function(e) {
        e.preventDefault();
        const action = e.currentTarget.name;
        const path = $(this).val();
        console.log(path);
        const fileName = path.split(/[\\\/]/).pop();
        let title, body;

        // Modal appears; prompt user for input
        switch (action) {
            case 'deleteFile':
                title = 'Delete';
                body = `<form id="inModal">
                            <div class="form-group">
                            <label class="col-form-label">Confirm deleting ${fileName}?</label>
                            </div>
                            <div id="messageDiv" class="alert alert-dismissible fade show mt-3" style="display: none; ">
                                <input type="hidden" class="close" data-dismiss="alert">
                                <strong id="messageText"></strong>
                            </div>
                        </form>`;
                break;
            case 'renameFile':
                title = 'Rename';
                body = `<form id="inModal">
                            <div class="form-group">
                            <label class="col-form-label">Rename to:</label>
                            <input required type="text" class="form-control" id="renameName" value="${fileName}">
                            </div>
                            <div id="messageDiv" class="alert alert-dismissible fade show" style="display: none; ">
                                <input type="hidden" class="close" data-dismiss="alert">
                                <strong id="messageText"></strong>
                            </div>
                        </form>`;
                break;
            case 'makeNewFolder':
                title = 'Create New Folder';
                body = `<form id="inModal">
                            <div class="form-group">
                            <label class="col-form-label">New folder name:</label>
                            <input required type="text" class="form-control" id="newFolderName" value="New folder">
                            </div>
                            <div id="messageDiv" class="alert alert-dismissible fade show" style="display: none; ">
                                <input type="hidden" class="close" data-dismiss="alert">
                                <strong id="messageText"></strong>
                            </div>
                            <input type="submit" style="display: none; " >
                        </form>`;
                break;
            default:
                break;
        }

        $('.modal-title').text(title);
        $('.modal-body').html(body);

        const modalConfirmmation = e => {
            e.preventDefault();
            $('#messageDiv').removeClass(
                'alert-danger alert-primary'
            );
            let data;
            switch (action) {
                case 'deleteFile':
                    data = {
                        path: path
                    }
                    break;
                case 'makeNewFolder':
                    data = {
                        path: path + '/' +  $('#newFolderName').val()
                    }
                    break;
                case 'renameFile':
                    data = {
                        srcPath: path,
                        destPath: $('#renameName').val()
                    }
                    break;
                default:
                    break;
            }

            // User input completed; post request to server
            $.ajax({
                type: "POST",
                data: data,
                url: "/FileManager/actions/" + action + ".php",
                dataType: 'json',
                success: function(result) {
                    console.log(result.msg);
                    // Important!
                    if (result.success) {
                        $('#modalConfirm').unbind('click');
                    }
                    $('#messageText').text(result.msg);
                    $('#messageDiv').addClass(
                        result.success ? 'alert-primary' : 'alert-danger'
                    );
                    if (result.success) {
                        $('#messageDiv').fadeTo(500, 500, () => { location.reload() });
                    } else {
                        $('#messageDiv').fadeTo(2000, 500).slideUp(500, function(){
                            $("#messageDiv").hide();
                            $('#messageDiv').removeClass(
                                'alert-danger primary'
                            );
                        });
                    }
                },
                error: function(result) {
                    console.log(result);
                }
            });
        }
        // MODAL CONFIRMATION
        $('#modalConfirm').click(modalConfirmmation);
        $('#inModal').submit(modalConfirmmation);
    });

    $('.copyMoveFile').click((e) => {
        e.preventDefault();
        form.attr('action', './copyMovePage.php');
        formSubmission(e.currentTarget.value);
    });

    $('.downloadFile').click((e) => {
        e.preventDefault();
        form.attr('action', './actions/download.php');
        formSubmission(e.currentTarget.value);
    });

    $('.changeDir').click((e) => {
        e.preventDefault();
        form.attr('action', '');
        formSubmission(e.currentTarget.value);
    });

    $('.preview').click((e) => {
        e.preventDefault();
        form.attr('action', './actions/viewFile.php');
        form.attr('target', '_blank');
        formSubmission(e.currentTarget.value);
        form.attr('target', '');
    });

    function formSubmission(pathValue) {
        form.append(jQuery('<input>', {
            'name': 'path',
            'value': pathValue,
            'id': 'tempInput'
        }));
        form.submit();

        $('#tempInput').remove();
        form.attr('action', '');
    }

    $('#uploadFile').on('change', () => {
        let files = $('#uploadFile').get(0).files;
        let fileNames = '<ul class="list-group">';
        for (let i = 0; i < files.length; i++) {
            fileNames += '<li class="list-group-item d-flex justify-content-between align-items-center">';
            fileNames += files[i].name;
            fileNames += '</li>';
        }
        fileNames += '</ul>';
        console.log(files)
        $('#fileList').html(fileNames);
    });
})