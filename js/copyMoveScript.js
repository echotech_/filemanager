$(document).ready(function() {
    const form = $('#copyMoveForm');

    $('.newPath').click((e) => {
        e.preventDefault();
        form.attr('action', '');
        form.append(jQuery('<input>', {
            'name': 'newPath',
            'value': e.currentTarget.value,
            'id': 'tempInput'
        }));
        
        form.submit();
        $('#tempInput').remove();
        form.attr('action', '');
    });

    $('.cancelCopyMove').click((e) => {
        e.preventDefault();
        form.attr('action', './index.php');
        form.append(jQuery('<input>', {
            'name': 'path',
            'value': event.currentTarget.value,
            'id': 'tempInput'
        }));
        form.submit();

        $('#tempInput').remove();
        form.attr('action', '');
    });

    $('.confirmCopyMove').click((e) => {
        e.preventDefault();
        const action = './actions/' + e.currentTarget.name + '.php';
        form.attr('action', action);
        form.submit();
        form.attr('action', '');
    });
})
