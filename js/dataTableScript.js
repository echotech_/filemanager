// Main data table
// Docs: https://datatables.net/manual/
let table;

$(document).ready( function () {
    table = $('#dataTable').DataTable({
        paging: false,
        info: false,
        columnDefs: [ 
            {
                searchable: false,
                targets: [0, 2, 3, 4]
            }, 
            {
                orderable: false,
                targets: [0, 4]
            },
            {
                'targets': 0,
                'render': function(data, type, row, meta) {
                    if (type === 'display') {
                        data = '<div class="checkbox checkbox-primary"><input type="checkbox" class="dt-checkboxes"><label></label></div>';
                    }
                    return data;
                },
                'checkboxes': {
                    'selectRow': true,
                    'selectAllRender': '<div class="checkbox checkbox-primary"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                }
            }
        ],
        'select': {
            'style': 'multi'
        },
        responsive: false,
    });

    $('.bulkAction').click((e) => {
        const rows_selected = table.column(0).checkboxes.selected();
        // Iterate over all selected checkboxes
        $.each(rows_selected, function(index, rowId){
            // Create a hidden element
            $('#datatableForm').append(
                $('<input>')
                    .attr('type', 'hidden')
                    .attr('name', 'paths[]')
                    .val(rowId)
            );
        });
    });
});