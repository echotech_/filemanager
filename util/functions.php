<?php
// for use in redirecting in homepage
function getPath(string $path) {
    $userPath = 'storage/' . $_SESSION['userName'];
    $userPath = str_replace('\\', '/', $userPath);
    $path = str_replace('\\', '/', $path);
    return safePath($path)? $path : $userPath;
}

// also will be used in file handling (moving, copying, etc.)
function safePath(string $path): bool {
    $userPath = 'storage/' . $_SESSION['userName'];
    // security: only redirect if non-malicious
    // i.e. WITHOUT '..' AND MUST UNDER USERPATH
    return !strpos($path, '..') &&
            strpos($path, $userPath) === 0;
}

function getPathSize(DirectoryIterator $path): string {
    if ($path->isDir()) {
        return "FOLDER";
    }

    $size = '';

    try {
        $size = $path->getSize();
    } catch (Exception $e) {
        return "";
    }

    if ($size < 1000) {
        return sprintf('%s B', $size);
    } elseif (($size / 1024) < 1000) {
        return sprintf('%s KB', round(($size / 1024), 2));
    } elseif (($size / 1024 / 1024) < 1000) {
        return sprintf('%s MB', round(($size / 1024 / 1024), 2));
    } elseif (($size / 1024 / 1024 / 1024) < 1000) {
        return sprintf('%s GB', round(($size / 1024 / 1024 / 1024), 2));
    } else {
        return sprintf('%s TB', round(($size / 1024 / 1024 / 1024 / 1024), 2));
    }
}
   
function getModTime(DirectoryIterator $path): string {
    date_default_timezone_set("Asia/Kuala_Lumpur"); 
    $result = "";

    try {
        $result = date('d/m/Y g:i a', $path->getMTime());
    } catch (Exception $e) {
        return "";
    }
    
    return $result; 
}

function downloadItem(string $path) {
    if (is_file($path)) {
        downloadFile($path);
    } elseif (is_dir($path)) {
        downloadZip($path);
    }
}

function downloadFile(string $path) {
    if (is_file($path)) {
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');//change your extension of your files
        header('Content-Disposition: attachment; filename="'.basename($path).'"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        readfile($path);
        exit;
    }
}

function downloadZip(string $path) {
    if (is_dir($path) && !is_dir_empty($path)) {
        ob_end_clean();
        // Get real path for our folder
        $rootPath = realpath($path);
        
        $zip_file = basename($path) . ".zip";
    
        // Initialize archive object
        $zip = new ZipArchive();
        $zip->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE);
        
        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY);
        
        foreach ($files as $name => $file) {
            // Get real and relative path for current file
            $filePath = $file->getRealPath();
            $relativePath = substr($filePath, strlen($rootPath) + 1);
        
            if (!$file->isDir())
            {
                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            } else {
                if($relativePath !== false) {
                    $zip->addEmptyDir($relativePath);
                }
            }
        }
    
        // Zip archive will be created only after closing object
        $zip->close();
        
        header('Content-Description: File Transfer');
        header('Content-Type: application/zip');
        header('Content-Disposition: attachment; filename='.basename($zip_file));
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($zip_file));
        readfile($zip_file);
        unlink($zip_file);
    } else {
        header('Location: ../index.php');
    }
}

function is_dir_empty($dir) {
    if (!is_readable($dir)) return NULL; 
    return (count(scandir($dir)) == 2);
}


function rcopydir($src, $dst) {
    $dir = opendir($src);
    mkdir($dst);
    while(false !== ( $file = readdir($dir)) ) {
        if (( $file != '.' ) && ( $file != '..' )) {
            if ( is_dir($src . '/' . $file) ) {
                rcopydir($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file,$dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

function rrmdir($dir) { 
    if (is_dir($dir)) { 
        $objects = scandir($dir);
        foreach ($objects as $object) { 
            if ($object != "." && $object != "..") { 
                if (is_dir($dir. DIRECTORY_SEPARATOR .$object) && !is_link($dir."/".$object))
                    rrmdir($dir. DIRECTORY_SEPARATOR .$object);
                else
                    unlink($dir. DIRECTORY_SEPARATOR .$object); 
            } 
        }
        rmdir($dir); 
    } 
}