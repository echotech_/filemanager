<?php require('./ui_components/htmlHeadMin.php'); ?>
<body>
<?php require('./ui_components/navbar.php'); ?>

<div class="container mx-auto mt-5" style="max-width: 400px;">
  <h2>Login Form</h2>
  <p><small>Please fill in the login details.<small></p>
  <form method="post" action="./actions/loginAction.php">
    <fieldset>
    <div class="form-group">
        <label for="username"><h5>Username</h5></label>
        <input name="username" type="username" class="form-control" id="username" aria-describedby="username" placeholder="Enter username">
    </div>
    <div class="form-group">
        <label for="password"><h5>Password</h5></label>
        <input name="password" type="password" class="form-control" id="password" placeholder="Password">
        <small id="reminder" class="form-text text-muted">Please do not share your password with anyone.</small>
    </div>
    <button type="submit" class="btn btn-primary"><h6>Login</h6></button>
    </fieldset>
  </form>
<?php if(@$_GET['Empty'] || @$_GET['Invalid']): ?>
  <div id="alert" class="alert alert-dismissible alert-danger mx-auto mt-3" style="display: none;">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>
      <?php if(@$_GET['Empty']): ?>
          Do not leave the details blank
      <?php elseif(@$_GET['Invalid']): ?>
          Wrong Username or Password
      <?php endif; ?>
    </strong>
  </div>
<?php endif; ?>
</div>

<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous">
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="./js/loginScript.js"></script>
</body>
</html>